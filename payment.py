# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta
import os


class Journal(metaclass=PoolMeta):
    __name__ = 'account.payment.journal'

    @classmethod
    def __setup__(cls):
        super(Journal, cls).__setup__()
        cls.confirming_payable_flavor.selection.append(
            ('n68', 'Standard 68'))


class Group(metaclass=PoolMeta):
    __name__ = 'account.payment.group'

    @classmethod
    def _get_confirming_template_paths(cls):
        values = super(Group, cls)._get_confirming_template_paths()
        values.update({
            'n68.txt': os.path.join(os.path.dirname(__file__),
                'template')
            })
        return values
