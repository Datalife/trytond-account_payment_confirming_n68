# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.tests.test_tryton import ModuleTestCase


class AccountPaymentConfirmingN68TestCase(ModuleTestCase):
    """Test Account Payment Confirming N68 module"""
    module = 'account_payment_confirming_n68'


del ModuleTestCase
